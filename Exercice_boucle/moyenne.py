def moyenne():
    notes = [14, 9, 9, 8, 12]
    moyenne = 0
    for note in notes :
        moyenne += note
    moyenne /= len(notes)
    print(f"{moyenne:.2f}" )