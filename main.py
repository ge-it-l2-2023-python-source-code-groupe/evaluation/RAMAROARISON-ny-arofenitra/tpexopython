from Exercice_variables import nombre_friedman,Operations,operationsConversions
from Exercice_affichage import Affichage,EcritureFormatee,EcritureFormatee2,PolyA,PolyApolyGC
from Exercice_Listes import jourDeLaSemaineListes,NombrePairesListes,Saisons,TablesDeMultiplication, ListRange, ListIndice
from Exercice_Teste import brinADN,devinette,frequenceDesAcideAmine,jourDeLaSemaineTeste,minimum,moyenne2, NbPremierMethode1,NbPremierMethode2,Nombre_paire, pairEtImpair, structureAcideAmine,Syracus
from Exercice_boucle import BoucleMethodes1, BoucleMethodes2, Fibonacci, JourDeLaSemaine6, matrice, matrice2, matriceDiagonal, minimumBoucle, moyenne, Nb1a10, nombre_consecutifs, Nombre_paire, pyramide, sautDePuce, semaine, triangle, triangleGauche, triangleInverse


choice="1"
while (choice!="0"):
    print("\n\n\n------variable------\n***2.11.1***Nombre du friedman\n***2.11.2***prediction\n***2.11.3***conversion du type")
    print("------affichage-------\n***3.6.1***affichage resultat de l addition\n***3.6.2***poly-A\n***3.6.3***poly-A et poly G-C\n***3.6.4***ecriture formate\n***3.6.5***ecriture formater2")
    print("------liste--------\n***4.10.1***jour de la semaine\n***4.10.2***saison\n***4.10.3***table de multiplication\n***4.10.4***nombre paire\n***4.10.5***liste and indixe\n***4.10.6***liste and range")
    print("------boucle et comparaison------\n***5.4.1***bocle de base\n***5.4.2***boucle et jour de la semaine\n***5.4.3***nombre de 1 a 10 sur une ligne\n***5.4.4***nombre paire et impaire\n***5.4.5***calcul de moyenne\n***5.4.6***produit nombre consecutif\n***5.4.7***triangle\n***5.4.8***triangle inverser\n***5.4.9***triangle gauche\n***5.4.10***pyramide\n***5.4.11***parcour de matrice\n***5.4.12***parcour de demi matrice\n***5.4.13***saut de puce\n***5.4.14***suite de fibonacci")
    print("------test--------\n***6.7.1***jour de la semaine\n***5.7.2***Sequence complementaire d'un brin d'ADN\n***6.7.3*** Minimum d'une liste\n***6.7.4*** Fréquence des acides aminés\n***6.7.5***Notes et mention d'un étudiant\n***6.7.6*** Nombres pairs\n***6.7.7*** Conjecture de Syracuse\n***6.7.8***Attribution de la structure secondaire des acides aminés d'une protéine\n***6.7.9*** Détermination des nombres premiers inférieurs à 100\n***6.7.10***Recherche d'un nombre par dichotomie")
    print("toute autre choix peut terminer le progreme")
    choice = input("entrer votre choix?")
    print("\n\n\n")
    if(choice=="2.11.1"):
        print("-----2.11.1-----")
        print("nombre friedman")
        nombre_friedman.nombre_friedman()
        
    elif(choice=="2.11.2"):
        print("----2.11.2------")
        print("operation")
        Operations.Operations()
        
    elif(choice=="2.11.3"):
        print("----2.11.3------")
        print("operatin et cinvertion")
        operationsConversions.operationsConversions()
        
    elif(choice=="3.6.1"):
        print("---3.6.1---")
        print("affichage")
        Affichage.Affichage()
        
    elif(choice=="3.6.2"):
        print("---3.6.2---")
        print("poly A")
        PolyA.PolyA()
        
    elif(choice=="3.6.3"):
        print("---3.6.3---")
        print("poly A poly GC")
        PolyApolyGC.PolyAployGC()
        
    elif(choice=="3.6.4"):
        print("---3.6.4---")
        print("ecriture formater ")
        EcritureFormatee.EcritureFormatee()
        
    elif(choice=="3.6.5"):
        print("---3.6.5---")
        print("ecriture formater2")
        EcritureFormatee2.EcritureFormatee2()
    elif(choice=="4.10.1"):
        print("---4.10.1")
        print("jour de la semaine")
        jourDeLaSemaineListes.jourDeLaSemaineListes()
    elif(choice=="4.10.2"):
        print("---4.10.2---")
        print("saison")
        Saisons.Saisons()
    elif(choice=="4.10.3"):
        print("---4.10.3---")
        print("table de multiplication")
        TablesDeMultiplication.TablesDeMultiplication()
    elif(choice=="4.10.4"):
        print("---4.10.4---")
        print("nombre paire liste")
        NombrePairesListes.NombrePairesListes()
    elif(choice=="4.10.5"):
        print("---4.10.5---")
        print("liste indice")
        ListIndice.ListIndice()
    elif(choice=="4.10.6"):
        print("---4.10.6---")
        print("liste range")
        ListRange.ListRange()
    elif(choice=="5.4.1"):
        print("---5.4.1---")
        print("boucle methode 1")
        BoucleMethodes1.BoucleMethodes1()
    elif(choice=="5.4.1"):
        print("---5.4.1---")
        print("boucle methode 2")
        BoucleMethodes2.BoucleMethodes2()
    elif(choice=="5.4.2"):
        print("---5.4.2---")
        print("jour de la semaine")
        JourDeLaSemaine6.JourDeLaSemaine6()
    elif(choice=="5.4.3"):
        print("---5.4.3---")
        print("nombre 1 a 10")
        Nb1a10.Nb1a10()
    elif(choice=="5.4.4"):
        print("---5.4.4---")
        print("nombre paire ")
        Nombre_paire.Nombre_paire()
    elif(choice=="5.4.5"):
        print("---5.4.5---")
        print("moyenne")
        moyenne.moyenne()
    elif(choice=="5.4.6"):
        print("---5.4.6---")
        print("nombre consecutif")
        nombre_consecutifs.nombre_consecutifs()
    elif(choice=="5.4.7"):
        print("---5.4.7---")
        print("triangle")
        triangle.triangle()
    elif(choice=="5.4.8"):
        print("---5.4.8---")
        print("triangle inverser")
        triangleInverse.triangleInverse()
    elif(choice=="5.4.9"):
        print("---5.4.9---")
        print("triangle gauche")
        triangleGauche.triangleGauche()
    elif(choice=="5.4.10"):
        print("---5.4.10---")
        print("pyramyde")
        pyramide.pyramide()
    elif(choice=="5.4.11"):
        print("---5.4.11---")
        print("matrice 2")
        matrice2.matrice2()
    elif(choice=="5.4.12"):
        print("---5.4.12---")
        print("matrice")
        matrice.matrice()    
    elif(choice=="5.4.12"):
        print("---5.4.12---")
        print("matrice diagonal")
        matriceDiagonal.matriceDiagonal()
    elif(choice=="5.4.13"):
        print("---5.4.13---")
        print("status de puce")
        sautDePuce.sautDePuce()
    elif(choice=="5.4.14"):
        print("---5.4.14---")
        print("fibonaci")
        Fibonacci.Fibonacci()
    elif(choice=="6.7.1"):
        print("---6.7.1---")
        print("jour de la semaine")
        jourDeLaSemaineTeste.jourDeLaSemaineTeste()
    elif(choice=="6.7.2"):
        print("---6.7.2---")
        print("brin adn")
        brinADN.brinADN()
    elif(choice=="6.7.3"):
        print("---6.7.3---")
        print("minimun liste")
        minimum.minimum()
    elif(choice=="6.7.4"):
        print("---6.7.4---")
        print("frequence des acide amine")
        frequenceDesAcideAmine.frecquenceDesAcideAmine()
    elif(choice=="6.7.5"):
        print("---6.7.5---")
        print("moyenne eleve")
        moyenne2.moyenne2()
    elif(choice=="6.7.6"):
        print("---6.7.6---")
        print("nombre paire")
        Nombre_paire.Nombre_paire()
    elif(choice=="6.7.7"):
        print("---6.7.7---")
        print("syracus")
        Syracus.Syracus()
    elif(choice=="6.7.8"):
        print("---6.7.8---")
        print("structure des acide amine")
        structureAcideAmine.structureAcideAmine()
    elif(choice=="6.7.9"):
        print("---6.7.9---")
        print("nombre premier methode")
        NbPremierMethode1.NbPremierMethode1()
    elif(choice=="6.7.9"):
        print("---6.7.9---")
        print("nombre methode 2")
        NbPremierMethode2.NbPremierMethode2()
    elif(choice=="6.7.10"):
        print("---6.7.10---")
        print("devinette")
        devinette.devinette()
    else:
        print("Saisie eroner ou vous avez quiter du programe")
        break
    input("appuyer Entrer pour continuer")