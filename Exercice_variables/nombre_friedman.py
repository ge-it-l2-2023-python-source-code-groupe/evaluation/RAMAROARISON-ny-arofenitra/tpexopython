def nombre_friedman():
    print(f"Le résultat du nombre de Friedman 7+3**6 = {7+3**6}")
    print(f"Le résultat du nombre de Friedman (3+4)**3 = {(3+4)**3}")
    print(f"Le résultat du nombre 3**6-5 = {3**6-5} n'est pas un nombre de Friedman")
    print(f"Comme (1+2**8)*5 = {(1+2**8)*5}, le nombre {(1+2**8)*5} est un nombre de Friedman")
    print(f"Comme (2+1**8)**7 = {(2+1**8)**7}, le nombre {(2+1**8)**7} est un nombre de Friedman")