def Saisons():
    hiver = ["décembre", "janvier", "février"]
    printemps = ["mars", "avril", "mai"]
    été = ["juin", "juillet", "août"]
    automne = ["septembre", "octobre", "novembre"]
    saison = [hiver, printemps, été, automne]

    print(saison[2])

    print(saison[1][0])

    print(saison[1:2])

    print(saison[:][1])

    print(list(range(10, 0, -1)))
