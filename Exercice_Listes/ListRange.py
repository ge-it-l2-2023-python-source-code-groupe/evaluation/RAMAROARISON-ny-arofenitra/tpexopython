def ListRange():
    lstVide = []
    lstFlottant = [0.0, 0.0, 0.0, 0.0, 0.0]

    print("lstVide :", lstVide)
    print("lstFlottant :", lstFlottant)

    for i in range(0, 1001, 200):
        lstVide.append(i)

    for i in range(0, 4):
        print(f"range(0, 4)[{str(i)}] = {i}")

    for i in range(4, 8):
        print(f"range(4, 8)[{str(i)}] = {i}")

    for i in range(2, 9, 2):
        print(f"range(2, 9, 2)[{str(i)}] = {i}")

    lstElmnt = list(range(0, 6))

    lstElmnt += lstVide
    lstElmnt += lstFlottant

    print("lstElmnt :", lstElmnt)
