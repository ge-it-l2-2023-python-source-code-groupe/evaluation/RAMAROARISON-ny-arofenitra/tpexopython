def moyenne2():
    notes = [14, 9, 13, 15, 12]

    note_max = max(notes)
    note_min = min(notes)
    moyenne = sum(notes) / len(notes)

    print(f"Note maximale : {note_max}")
    print(f"Note minimale : {note_min}")
    print(f"Moyenne : {moyenne:.2f}")

    mention = ""
    if 10 <= moyenne < 12:
        mention = "Passable"
    elif 12 <= moyenne < 14:
        mention = "Assez bien"
    elif moyenne >= 14:
        mention = "Bien"

    print(f"Mention obtenue : {mention}")