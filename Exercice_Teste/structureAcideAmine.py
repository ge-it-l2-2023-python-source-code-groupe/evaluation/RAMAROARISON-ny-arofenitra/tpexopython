def structureAcideAmine():
    angles_phi_psi = [
        [48.6, 53.4], [-124.9, 156.7], [-66.2, -30.8],
        [-58.8, -43.1], [-73.9, -40.6], [-53.7, -37.5],
        [-80.6, -26.0], [-68.5, 135.0], [-64.9, -23.5],
        [-66.9, -45.5], [-69.6, -41.0], [-62.7, -37.5],
        [-68.2, -38.3], [-61.2, -49.1], [-59.7, -41.1]
    ]

    for i, angles in enumerate(angles_phi_psi, start=1):
        phi, psi = angles
        is_in_helix = (phi >= -87 and phi <= -27) and (psi >= -77 and psi <= -17)

        print(f"{i} {angles} {'est' if is_in_helix else 'est pas'} en hélice")
        nombre_premier=2
