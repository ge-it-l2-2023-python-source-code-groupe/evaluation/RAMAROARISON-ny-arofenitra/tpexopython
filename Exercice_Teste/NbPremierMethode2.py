def NbPremierMethode2():
    premiers = [2]


    for i in range(3, 101):

        composite = False
        for p in premiers:
            if i % p == 0:
                composite = True
                break

        if not composite:
            premiers.append(i)

    print(premiers)
