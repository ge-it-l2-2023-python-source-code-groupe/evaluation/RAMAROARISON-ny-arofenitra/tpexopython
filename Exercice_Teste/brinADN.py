def brinADN():
    ADN = (["A", "C", "G", "T", "T", "A", "G", "C", "T", "A", "A", "C", "G"])
    complementary_ADN = []
    for complementary_ADN in ADN:
        if complementary_ADN == "A":
            print(f"{complementary_ADN} T", end="_")
        elif complementary_ADN == "T":
            print(f"{complementary_ADN} A", end="_")
        elif complementary_ADN == "C":
            print(f"{complementary_ADN} G", end="_")
        elif complementary_ADN == "G":
            print(f"{complementary_ADN} C", end="_")
        