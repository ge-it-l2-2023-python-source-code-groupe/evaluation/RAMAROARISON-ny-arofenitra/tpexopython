def frecquenceDesAcideAmine():
    frecquence = ["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]
    freq_A = 0
    freq_R = 0
    freq_W = 0
    freq_G = 0

    # Parcours de la séquence pour calculer la fréquence
    for amino_acide in frecquence:
        if amino_acide == "A":
            freq_A += 1
        elif amino_acide == "R":
            freq_R += 1
        elif amino_acide == "W":
            freq_W += 1
        elif amino_acide == "G":
            freq_G += 1

    # Affichage des résultats
    print("Fréquence d'alanine (A) :", freq_A)
    print("Fréquence d'arginine (R) :", freq_R)
    print("Fréquence de tryptophane (W) :", freq_W)
    print("Fréquence de glycine (G) :", freq_G)